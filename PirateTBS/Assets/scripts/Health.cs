﻿using UnityEngine;

public class Health : MonoBehaviour {

    TextMesh tm;
    MeshRenderer mr;
    public int total = 4;
    public char symbol = '=';
    int max;

    void Start () {
        tm = GetComponent<TextMesh>();
        mr = GetComponent<MeshRenderer>();
        max = total;
        for (int i = 0; i < total; i++)
            tm.text += symbol;     // health bar
    }
    
    void Update () {
        transform.forward = Camera.main.transform.forward;   // health bar always faces the Camera
    }
   
    // ship repaired
    public void Increase(int amt)
    {
        total += amt;
        if (total > max)
            total = max;
        tm.text = "";
        for (int i = 0; i < total; i++)
            tm.text += symbol;     // health bar
    }

    // take damage
    public bool Decrease(int amt) {

        total -= amt;
        if (total <= 0)
        {
            return true;   // done
        }
        else
        {
            tm.text = "";
            for (int i = 0; i < total; i++)
                tm.text += symbol;

            return false;  // not dead yet
        }
    }


}
