﻿using System.Collections;
using UnityEngine;

public class Cannonball : MonoBehaviour
{
    Vector3 originalPos;
    Vector3 target = Vector3.zero;
    bool engaged = false;
    GameObject explosion;
    static Pointer pointer;
    string launcher;

    private void Start()
    {
        originalPos = transform.position;
        explosion = Resources.Load("WFX-explosion") as GameObject;
        pointer = GameObject.Find("GameMgr").GetComponent<Pointer>();
    }

    public void Fire(GameObject atk, Vector3 dest)
    {
        launcher = atk.gameObject.name; // avoid self-damage

        // set up the firing line:
        transform.position = atk.transform.position;
        target = dest;
        engaged = true;
        StartCoroutine(FlyOver());
    }

    private IEnumerator FlyOver()
    {
        if (engaged == false)
            yield return null;

        bool arrived = false;
        while (!arrived)
        {
            if (engaged == false)
                break;

            Vector3 dist = target - transform.position;

            if (dist.magnitude < 0.01f)
            {
                arrived = true;
                engaged = false;
            }
            transform.Translate(dist * Time.deltaTime * 2f);
            yield return null;
        }
    }

    // hit some target
    private void OnTriggerEnter(Collider other)
    {
        if (!engaged)        // can hit only one target, no multiple hits in one line
            return;

        if (other.gameObject.name == launcher)
            return;    // avoid self-destruction

        //print("blowing up");
        Vector3 pos = new Vector3(transform.position.x, transform.position.y + 1f, transform.position.z);
        Instantiate(explosion, pos, Quaternion.identity);
        transform.position = originalPos;  // hide weapon
        engaged = false;
        pointer.SetIcon("none");

        Health otherHealth = other.gameObject.GetComponentInChildren<Health>();
        if (otherHealth != null)
            otherHealth.Decrease(1);

    }
}
