﻿using UnityEngine;

public class Player : MonoBehaviour
{
    public Ship[] fleet;

    void Start()
    {
        fleet = GetComponentsInChildren<Ship>();
    }

    // end of turn
    public void Reset()
    {
        foreach (Ship s in fleet)
            if (s != null)
            { // if not destroyed
                s.active = true;  // clear for next move
                s.Clear();
            }
    }

}
