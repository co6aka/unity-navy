﻿using UnityEngine;

// sets the cursor options: hand (move), crosshairs (shoot), cross (heal)
public class Pointer : MonoBehaviour
{
    public Texture2D crossHairs, heal, hand;
    Vector2 hotSpot;
    public static string option = "none";
    public void SetIcon(string action)
    {
        option = action;

        if (action == "none")             
            Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);  // set default pointer
        if (action == "aim")
            SetIcon(crossHairs);
        if(action == "heal")
            SetIcon(heal);
        if (action == "move")
            SetIcon(hand, 0.1f, 0.1f);
    }
    void SetIcon(Texture2D crs, float xHotspot=0.5f, float yHotspot=0.5f)
    {        
        hotSpot = new Vector2(crs.width * xHotspot, crs.height * yHotspot);  // corsshairs, with hotspot in the middle
        Cursor.SetCursor(crs, hotSpot, CursorMode.Auto);    
    }

}