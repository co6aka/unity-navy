﻿using UnityEngine;
using System.Collections;   // Coroutine

public class Ship : MonoBehaviour
{
    private Material originalMat;
    private Renderer rend;
    private float Y;
    float xRot;
    float zRot = 0.2f;

    public Material selectedMat;
    bool wobble;
    bool selected;

    Health health;
    static Pointer pointer;

    public char type;  // Battleship, Torpedo or Hospital
    GameObject cannon, explosion;
    Cannonball ball;

    // same target, just with our own Y-position so that our ship doesn't dive
    Vector3 destination;  
    public bool active = false;   // has not made a move in this turn yet
   
    void Start()
    {
        Y = transform.position.y;
        rend = GetComponent<Renderer>();
        originalMat = rend.material;

        health = GetComponentInChildren<Health>();
        xRot = Random.Range(0.01f, 0.03f);

        pointer = GameObject.Find("GameMgr").GetComponent<Pointer>();
        cannon = Resources.Load("WFX-cannon") as GameObject;
        explosion = Resources.Load("WFX-explosion") as GameObject;
        ball = GameObject.Find("Cannonball").GetComponent<Cannonball>();
    }

    void Update()
    {
        // always gently rock back and forth as if on waves
        if (transform.rotation.eulerAngles.x > 10f || transform.rotation.eulerAngles.x < -10f)
            xRot = -xRot;
        transform.Rotate(xRot, 0f, 0f );

        // when mouse hovers over, start wobbling to indicate it's selectable
        if (wobble)
        {
            if (transform.rotation.eulerAngles.z > 10f || transform.rotation.eulerAngles.z < -10f)
                zRot = -zRot;
            transform.Rotate(0f, 0f, zRot);
        }
    }

    // mouse click
    void OnMouseDown()
    {
        // toggle between original color and highlited color
        if (selected == false)
        {
            rend.material = selectedMat;
            selected = true;
        }
        else
            Clear();
    }

    public void Clear() // used by other scripts
    {
        rend.material = originalMat;
        selected = false;
    }

    // wobble on mouse over
    void OnMouseEnter()
    {      
        wobble = true;
    }

    void OnMouseExit()
    {
        wobble = false;
    }

    public void MoveTo(GameObject target)
    {
        destination = new Vector3(target.transform.position.x, Y, target.transform.position.z);
        active = false;     // done for this turn
        // actually move to a destination
        // ...

    }
    
    public void Attack(GameObject tgt)
    {
        if (!active)
            return;

        if (type == 'T')    // torpedo ship 
        {
            MoveTo(tgt);
            return;
        }

        active = false;

        // let's fire a cannonball staight at the enemy... if it hits a friendly or someone else along
        // the way, so be it :)
        Vector3 cannonPos = new Vector3(transform.position.x + 1, transform.position.y + 2, transform.position.z);
        Instantiate(cannon, cannonPos, Quaternion.identity);
        ball.Fire(gameObject, tgt.transform.position); // will hit the target or something on the way :)
    }


    public void Damage(int amt)
    {      
        Instantiate(explosion, transform.position, Quaternion.identity);

        bool dead = health.Decrease(amt);
        if(dead)
        {
            gameObject.GetComponent<Rigidbody>().useGravity = true;
            Destroy(gameObject, 1f);
        }
        // adjust health, and if none left, sink..
    }

    public void Repair(int amt)
    {
        // adjust health, up to the max amt
    }

    // ships collide
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Tile>() != null)  // ignore collisions with tiles
            return;

        if (other.gameObject.name.StartsWith("Cannonball"))
            return;  // cannonball does damage on its own, no need to go kamikaze 

        if (type == 'T') // hit by a torpedo ship...
        {
            Instantiate(explosion, transform.position, Quaternion.identity);
            // reduce health substantially, or possibly drown right away
            //Destroy(gameObject, 1f); 
            
        }
    }

}
